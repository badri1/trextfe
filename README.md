[![Build Status](https://travis-ci.org/badri/awesome-elm.svg?branch=master)](https://travis-ci.org/badri/awesome-elm)

> awesome-elm

## Installation

Make sure the following are installed:

* Elm (v0.16.0)
* NodeJs (and npm)
* Elm (`npm install -g elm@0.16.0`)
* Compass (for SASS) (`gem update --system && gem install compass`)

## For the first time...

1. npm install all the dependencies, like gulp and bower.

2. bower install the CSS and JS dependencies.

## Usage

1. Serve locally, and watch file changes:

`gulp`

2. Prepare file for publishing (e.g. minify, and rev file names):

`gulp publish`

3. Deploy to GitHub's pages (`gh-pages` branch of your repository):

`gulp deploy`

## Unit Tests

In order to view the tests on the browser Start elm reactor (elm-reactor) and navigate to http://0.0.0.0:8000/src/elm/TestRunner.elm

## License

MIT


## TODO

* change basic structure to heading-based.
* multiple key operations, like shift+ctrl+up
* table str(??)
* export to org
* cycle visibility
* mark a node
* add todo
* search/filter by tag/todo state
* add tags
* insert dates
* focus on current subtree
* create revisions/sync with dropbox

## Common operations

* move node up
* move node down
* delete node
* undo(unlimited)
* new sibling node
* new child node
* make node a child node
* cycle through global collapse and expand
* search nodes
* add tag
* periodic save
