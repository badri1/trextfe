module Trext (..) where

import Uuid
import Random.PCG exposing (generate, Seed, initialSeed2)
import Effects exposing (Effects)
import Keyboard
import Debug
import Char
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Signal exposing (Address)
import Task
import Markdown exposing (toHtml)

-- Model


type alias Model =
  { nodes : List Node
  , currentSeed : Seed
  , currentNode : Int
  }


type alias Node =
  { content : String
  , uuid : Maybe Uuid.Uuid
  , order : Int
  }


type alias NodeState =
  { uuid : String, content : String }


createNode : Int -> String -> Maybe Uuid.Uuid -> Node
createNode order content uuid =
  { content = content
  , uuid = uuid
  , order = order
  }


defaultNode : Node
defaultNode =
  createNode -1 "" Nothing


init : Seed -> ( Model, Effects Action )
init seed =
  ( initialModel seed
  , Effects.none
  )


initialModel : Seed -> Model
initialModel seed =
  { nodes =
      [ createNode 1 """## Vestibulum dapibus nunc ac augue.
Pellentesque posuere. Donec vitae orci sed dolor rutrum auctor. Donec posuere vulputate arcu. Vestibulum ullamcorper mauris at ligula. Cras id dui. Suspendisse potenti. Duis leo. Curabitur at lacus ac velit ornare lobortis. Etiam rhoncus.""" (Uuid.fromString "2f67eb3f-b7e7-4027-bb62-c9ac3d406d9b")
      , createNode 2 """Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Etiam sit amet orci eget eros faucibus tincidunt. Curabitur at lacus ac velit ornare lobortis.

Nullam quis ante. In auctor lobortis lacus. Praesent ac massa at ligula laoreet iaculis. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor.""" (Uuid.fromString "7756e649-3bd5-459b-84d9-a16d88d46b65")
      , createNode 3 """Morbi nec metus. Curabitur turpis. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum.""" (Uuid.fromString "e9d92621-2826-4963-876c-a7aee7fd2efb")
      , createNode 4 """Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Morbi mollis tellus ac sapien. Praesent ac sem eget est egestas volutpat. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.

Proin magna. Morbi ac felis. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Fusce convallis metus id felis luctus adipiscing.

Vivamus consectetuer hendrerit lacus. Vivamus in erat ut urna cursus vestibulum. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Sed aliquam ultrices mauris.""" (Uuid.fromString "f0204f10-932f-4bca-9ffc-92187529b584")
      ]
  , currentSeed = seed
  , currentNode = 0
  }



-- Update


type Action
  = NoOp
  | MoveUp
  | MoveDown
  | CurrentNode Int
  | UpdateNode
  | NewNode
  | UpdateContent NodeState
  | DummyAction


update : Action -> Model -> ( Model, Effects Action )
update action model =
  case action of
    NoOp ->
      Debug.log
        "no op"
        ( model, Effects.none )

    MoveUp ->
      let
        a =
          getNode model.currentNode model.nodes

        b =
          getNode (model.currentNode - 1) model.nodes

        newNodes =
          swapNodes (Maybe.withDefault defaultNode a) (Maybe.withDefault defaultNode b) model.nodes

        newCurrentNode =
          model.currentNode - 1
      in
        Debug.log
          "move up"
          ( { model
              | nodes = newNodes
              , currentNode =
                  if newCurrentNode >= 0 then
                    newCurrentNode
                  else
                    -1
            }
          , Effects.none
          )

    MoveDown ->
      let
        a =
          getNode model.currentNode model.nodes

        b =
          getNode (model.currentNode + 1) model.nodes

        newNodes =
          swapNodes (Maybe.withDefault defaultNode a) (Maybe.withDefault defaultNode b) model.nodes

        newCurrentNode =
          model.currentNode + 1
      in
        Debug.log
          "move down"
          ( { model
              | nodes = newNodes
              , currentNode =
                  if newCurrentNode >= (List.length model.nodes) then
                    newCurrentNode
                  else
                    -1
            }
          , Effects.none
          )

    CurrentNode order ->
      Debug.log
        "focus"
        ( { model | currentNode = order }, Effects.none )

    UpdateNode ->
      Debug.log
        "blur"
        ( model, Effects.none )

    UpdateContent updatedNode ->
      let
        currentNode =
          getCurrentNodeOrder updatedNode.uuid model.nodes
      in
        case currentNode of
          Nothing ->
            ( model, Effects.none )

          Just n ->
            ( { model | nodes = updateCurrentNode updatedNode.uuid updatedNode.content model.nodes, currentNode = n }, Effects.none )

    NewNode ->
      let
        ( newUuid, newSeed ) =
          generate Uuid.uuidGenerator model.currentSeed

        newNode =
          createNode (model.currentNode + 1) "" (Just newUuid)

        ( before, after ) =
          splitAt model.currentNode model.nodes

        sendTask =
          Signal.send focusNewNode.address (Uuid.toString newUuid)
            `Task.andThen` (\_ -> Task.succeed NoOp)
      in
        Debug.log
          "new node"
          ( { model
              | nodes = before ++ [ newNode ] ++ after
              , currentSeed = newSeed
              , currentNode = model.currentNode + 1
            }
          , sendTask |> Effects.task
          )
    DummyAction ->
      Debug.log
        "dummy action"
        ( model, Effects.none )



-- Signals


focusNewNode :
  { address : Signal.Address String
  , signal : Signal String
  }
focusNewNode =
  Signal.mailbox ""


ctrlUp : Signal Bool
ctrlUp =
  Signal.map2 (&&) Keyboard.ctrl (Keyboard.isDown <| 38)


ctrlDown : Signal Bool
ctrlDown =
  Signal.map2 (&&) Keyboard.ctrl (Keyboard.isDown <| 40)


ctrlEnter : Signal Bool
ctrlEnter =
  Signal.map2 (&&) Keyboard.ctrl Keyboard.enter

altJ : Signal Bool
altJ =
  Signal.map2 (&&) Keyboard.ctrl (Keyboard.isDown <| 84)


moveup : Signal Action
moveup =
  keyToAction ctrlUp MoveUp


movedown : Signal Action
movedown =
  keyToAction ctrlDown MoveDown


newnode : Signal Action
newnode =
  keyToAction ctrlEnter NewNode

emacs : Signal Action
emacs =
  keyToAction altJ DummyAction


keyToAction : Signal Bool -> Action -> Signal Action
keyToAction keyPressed action =
  let
    toAction b =
      case b of
        True ->
          action

        False ->
          NoOp
  in
    Signal.dropRepeats
      <| Signal.map toAction keyPressed



-- View


view : Address Action -> Model -> Html
view address model =
  div
    [ class "container" ]
    [ nodeList address model
    ]


nodeList : Address Action -> Model -> Html
nodeList address model =
  div [ class "org-mode", contenteditable True, spellcheck False ] (List.map (nodeItem address model.currentNode) model.nodes)


nodeItem : Address Action -> Int -> Node -> Html
nodeItem address cnode node =
  let
    uuid =
      case node.uuid of
        Nothing ->
          ""

        Just uuid ->
          Uuid.toString uuid
  in
    div
      [ classList [ ( "node", True ), ( "focus", node.order == cnode ) ]
      , id uuid
      , onClick address (CurrentNode node.order)
      , attribute "data-order" (toString node.order)
      ]
      [ toHtml node.content ]


swapNodes : Node -> Node -> List Node -> List Node
swapNodes a b nodes =
  if (a == b) || (a == defaultNode) || (b == defaultNode) then
    nodes
  else
    List.map
      (\node ->
        if node.order == a.order then
          { b | order = a.order }
        else if node.order == b.order then
          { a | order = b.order }
        else
          node
      )
      nodes


getNode : Int -> List Node -> Maybe Node
getNode order nodes =
  case nodes of
    [] ->
      Nothing

    x :: xs ->
      if x.order == order then
        Just x
      else
        getNode order xs


splitAt : Int -> List Node -> ( List Node, List Node )
splitAt order list =
  let
    step x ( trues, falses ) =
      if x.order <= order then
        ( x :: trues, falses )
      else
        ( trues, { x | order = x.order + 1 } :: falses )
  in
    List.foldr step ( [], [] ) list



-- update current node in node list


updateCurrentNode : String -> String -> List Node -> List Node
updateCurrentNode uuid content nodes =
  let
    newNodes =
      List.map
        (\x ->
          if x.uuid == Uuid.fromString uuid then
            { x | content = content }
          else
            x
        )
        nodes
  in
    newNodes


getCurrentNodeOrder : String -> List Node -> Maybe Int
getCurrentNodeOrder uuid nodes =
  case nodes of
    [] ->
      Nothing

    x :: xs ->
      if x.uuid == Uuid.fromString uuid then
        Just x.order
      else
        getCurrentNodeOrder uuid xs



-- TEST CODE
-- import Trext
-- import Random.PCG exposing (Seed, initialSeed2)
-- seed0 = initialSeed2 0 453145
-- i = Trext.initialModel seed0
-- nodes = i.nodes
-- contenteditable onkeyup cursor position.
-- textarea approach http://codepen.io/anon/pen/VeqaKM
