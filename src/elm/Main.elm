module Main (..) where

import Random.PCG exposing (Seed, initialSeed2)
import Effects exposing (Never)
import Trext exposing (init, update, view, moveup, movedown, newnode, Action, NodeState, focusNewNode, emacs)
import StartApp
import Task
import Keyboard


app =
  StartApp.start
    { init = init seed0
    , update = update
    , view = view
    , inputs = [ moveup, movedown, newnode, updateContentSignal, emacs ]
    }


main =
  app.html


port randomSeed : ( Int, Int )
seed0 : Seed
seed0 =
  (uncurry initialSeed2) randomSeed


port tasks : Signal (Task.Task Never ())
port tasks =
  app.tasks


lrarrows : Signal Bool
lrarrows =
  Signal.map2 (||) (Keyboard.isDown <| 37) (Keyboard.isDown <| 39)


udarrows : Signal Bool
udarrows =
  Signal.map2 (||) (Keyboard.isDown <| 38) (Keyboard.isDown <| 40)


arrows : Signal Bool
arrows =
  Signal.map2 (||) lrarrows udarrows


port getDom : Signal String
port getDom =
  Signal.map (always "updateRequest") <| arrows

port onCreate : Signal String
port onCreate = Signal.map (\x -> x) focusNewNode.signal


updateContentSignal : Signal Action
updateContentSignal =
  Signal.map
    (\content -> (Trext.UpdateContent content))
    updateContent


port updateContent : Signal NodeState
